export class Game{
    gameId: number;
    opponent: string;
    opponentId: number;
    time: Date;
    location: string;
    locationId: string;
    scoreDisplay: string;
    homeAway: string;


    constructor({gameId,opponent,opponentId,time,location,locationId,scoreDisplay,homeAway}){
        this.gameId = gameId;
        this.opponent = opponent;
        this.opponentId = opponentId;
        this.location = location;
        this.locationId = locationId;
        this.time = time;
        this.scoreDisplay = scoreDisplay;
        this.homeAway = homeAway;
    }
}
