export class Team{
    coach: string;
    division:string;
    id: number;
    name: string;

    constructor({ coach, division, id, name }){
        this.id = id;
        this.name = name;
        this.coach = coach;
        this.division = division;
    }
}