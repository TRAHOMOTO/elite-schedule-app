import {Component, Inject} from '@angular/core';
import {
  AlertController, LoadingController, ModalController, NavController, NavParams,
  ToastController
} from 'ionic-angular';

import { Vibration } from '@ionic-native/vibration';

import {TournamentsPage, TeamHomePage} from '../';
import {EliteAppService, eliteAppServiceProvider} from "../../services";
import {Team} from "../../beans";
import {FavTeam, UserSettingsService} from "../../services/user-settings.service";
import {CoordsModalPage} from "../coords-modal/coords-modal";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { LocalNotifications } from '@ionic-native/local-notifications';



@Component({
  selector: 'page-my-teams',
  templateUrl: 'my-teams.html'
})
export class MyTeamsPage {

  favorites: FavTeam[];
  imageSrc: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              @Inject(eliteAppServiceProvider) private dataStore: EliteAppService,
              private loading: LoadingController,
              private userSettings: UserSettingsService,
              private toaster: ToastController,
              private modalCtrl: ModalController,
              private alert: AlertController,
              private vibro: Vibration,
              private camera: Camera,
              private notify: LocalNotifications) {
    // do nothing
  }

  ionViewWillEnter(){
      this.refreshFavs();
  }

  favTapped(favItem) {

    this.loading.create({
      content: 'Загрузка ...',
      dismissOnPageChange: true
    })
    .present()
    .then(() => {
      this.dataStore.getTournamentData(favItem.tournamentId)
        .subscribe(() => {
          this.navCtrl.push(TeamHomePage, new Team(favItem.team))
        })
    });
  }

  goToTournaments() {
    this.navCtrl.push(TournamentsPage);
  }

  unfavoriteTeam(team: Team){
    this.alert.create({
      title: 'Удалить?',
      subTitle: 'Удалить комманду из своего списка избранных?',
      buttons: [
        {text: 'Да', handler: () => this.unfavorite(team)},
        {text: 'Нет'}
      ]
    }).present();
  }

  vibrate(){
    this.vibro.vibrate(1000);
  }

  vibratePattern(){
    this.vibro.vibrate([1000,2000,2000,2000]);
  }

  getCoords(){
    this.modalCtrl.create(CoordsModalPage).present();
  }

  getPicture(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: false
    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.imageSrc = imageData;
    }, (err) => {
      // Handle error
    });
  }

  notification(){
    this.notify.schedule({
      id: 1,
      text: 'Single ILocalNotification',
      led: 'FF0000',
      // sound: isAndroid? 'file://sound.mp3': 'file://beep.caf',
      data: { secret: 123 }
    });
  }


  private unfavorite(team: Team) {
    this.userSettings
        .unfavoriteTeam(team)
        .then(() => this.toaster.create({
          message: 'Удалено успешно',
          duration: 2000
        }).present())
        .then(() => this.refreshFavs());
  }

  private refreshFavs() {
    this.userSettings.getFavoriteTeams()
        .subscribe(teams => this.favorites = teams);
  }
}
