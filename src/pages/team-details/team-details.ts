import {Component, Inject} from "@angular/core";
import {AlertController, NavController, NavParams, Refresher, ToastController} from "ionic-angular";
import {EliteAppService, eliteAppServiceProvider} from "../../services";
import {Game, Team} from "../../beans";
import {GamePage} from "../";
import {FavTeam, UserSettingsService} from "../../services";

import {Observable} from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/add/operator/do";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/reduce";
import "rxjs/add/operator/pluck";
import * as moment from "moment";


@Component({
    selector: 'page-team-details',
    templateUrl: 'team-details.html'
})
export class TeamDetailsPage {

    team: Team;
    teamGames$: Observable<Game[]>;
    teamStatistic: { wins, losses };

    filterValue: string;
    diteFilterEnabled = false;
    isFavorite = false;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                @Inject(eliteAppServiceProvider) private dataStore: EliteAppService,
                private alertCtl: AlertController,
                private toaster: ToastController,
                private userSettings: UserSettingsService) {

        this.team = this.navParams.data;
    }

    ionViewDidEnter() {
        this.teamGames$ = this.teamGamesSource();
        this.dataStore
            .getLastTourneyData()
            .switchMap(data => Observable.from(data.standings))
            .filter(item => item['teamId'] === this.team.id)
            .subscribe(stat => this.teamStatistic = <{ wins, losses }>stat);

        this.userSettings.getFavoriteTeams()
            .subscribe((teams: FavTeam[]) => (this.isFavorite = !!teams.find((ft) => ft.team.id === this.team.id)));
    }

    private teamGamesSource(): Observable<Game[]> {
        return this.dataStore
            .getLastTourneyData()
            .pluck('games')
            .switchMap((games: ArrayLike<any>) => Observable.from(games))
            .filter((game: { team1Id, team2Id }) => game.team1Id === this.team.id || game.team2Id === this.team.id)
            .filter((game: { time }) =>
                (!this.diteFilterEnabled || !this.filterValue)
                || (this.diteFilterEnabled && moment(game.time).isSame(this.filterValue, 'day'))
            )
            .map((gameRaw: { id, team2, team1, team2Id, team1Id }) => {
                const isTeam1 = (gameRaw.team1Id === this.team.id);

                return new Game({
                    gameId: gameRaw.id,
                    opponent: (isTeam1 ? gameRaw.team2 : gameRaw.team1),
                    opponentId: (isTeam1 ? gameRaw.team2Id : gameRaw.team1Id),
                    time: new Date(gameRaw['time']),
                    location: gameRaw['location'],
                    locationId: gameRaw['locationId'],
                    scoreDisplay: TeamDetailsPage.formatScore(isTeam1, gameRaw['team1Score'], gameRaw['team2Score']),
                    homeAway: isTeam1 ? 'vs.' : 'at'
                });
            })
            .reduce((accum: any, game) => [...accum, ...game], []);
    }

    scoreDisplayBadge(game: Game) {
        return game.scoreDisplay ? game.scoreDisplay.charAt(0) : '';
    }

    scoreDisplay(game: Game) {
        return game.scoreDisplay ? (<string>game.scoreDisplay).substr(2) : '';
    }

    scoreDisplayBadgeColor(game: Game) {
        return this.scoreDisplayBadge(game) === 'L' ? 'danger' : 'primary';
    }

    itemTapped(ev, game: Game) {
        const originalGame$ = this.dataStore.getLastTourneyData()
            .map(data => data.games)
            .switchMap(games => Observable.from(games))
            .filter((g: { id }) => g.id === game.gameId);

        this.navCtrl.parent.parent.push(GamePage, originalGame$);
    }

    dateChanged() {
        this.teamGames$ = this.teamGamesSource();
    }

    toggleFavorite() {
        if (this.isFavorite) {
            this.alertCtl.create({
                title: 'Удалить?',
                subTitle: 'Удалить комманду из своего списка избранных?',
                buttons: [
                    {text: 'Да', handler: () => this.deleteFromFavorite()},
                    {text: 'Нет'}
                ]
            }).present();
        } else {
            this.addToFavorite();
        }
    }

    refreshTournamentData(refr: Refresher){
        this.dataStore
            .refreshLastTourneyData()
            .subscribe(() => {
                this.ionViewDidEnter();
                refr.complete();
            });
    }

    private static formatScore(isTeam1: boolean,
                               team1Score: number,
                               team2Score: number): string {

        if (team1Score && team2Score) {
            const teamScore = (isTeam1 ? team1Score : team2Score);
            const opponentScore = (isTeam1 ? team2Score : team1Score);
            const winIndicator = ((teamScore > opponentScore) ? 'W: ' : 'L: ');
            return winIndicator + teamScore + '-' + opponentScore;
        } else {
            return '';
        }
    }

    private deleteFromFavorite() {
        this.userSettings
            .unfavoriteTeam(this.team)
            .then(() => this.showToaster('Удалено успешно'));

        this.isFavorite = false
    }

    private addToFavorite() {
        this.dataStore
            .getLastTourneyData()
            .pluck("tournament")
            .subscribe((tourney: { id, name }) => {
                this.userSettings
                    .favoriteTeam(this.team, tourney.id, tourney.name)
                    .then(() => this.showToaster('Добавлено успешно'));
            });


        this.isFavorite = true;
    }

    private showToaster(message: string) {
        this.toaster.create({
            message: message,
            duration: 1500
        }).present();
    }
}
