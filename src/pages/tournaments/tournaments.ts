import {Component, Inject} from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {TeamsPage} from "../";
import {Observable} from "rxjs";
import {eliteAppServiceProvider} from "../../services";
import {TournamentTeaser} from "../../beans";

/*
 Generated class for the Tournaments page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-tournaments',
  templateUrl: 'tournaments.html'
})
export class TournamentsPage {
  tournaments$: Observable<TournamentTeaser[]>;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              @Inject(eliteAppServiceProvider) private dataStore,
              private loading: LoadingController) {
  }

  ionViewDidLoad() {

    const loader = this.loading.create({
      content: "Загрузка..."
    });
    loader.present().then(() => {
      this.tournaments$
        = this.dataStore.getTournaments()
        .do(() => loader.dismiss());
    });

  }

  itemTapped(tourn: { id, name }) {
    this.navCtrl.push(TeamsPage, tourn);
  }
}
