import { Component, Inject } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Game} from "../../beans/";
import {eliteAppServiceProvider} from "../../services/index";
import {EliteAppService} from "../../services/elite-app.service";

export type MapModel = {lat, lng, zoom, content};
declare var window: any;

@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {
  game: Game;
  map: MapModel;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              @Inject(eliteAppServiceProvider) private dataStore: EliteAppService) {
    // do nothing
  }

  ionViewDidLoad() {
    this.game = this.navParams.data;
    this.gameLocationSource(this.game)
    .first()
    .subscribe((data: {latitude, longitude, name}) => {
      this.map = {
        lat: data.latitude,
        lng: data.longitude,
        zoom: 12,
        content: data.name
      };
    });
  }

  mapClicked(ev: {coords: {lat, lng}}){
    console.log(ev.coords.lat, ev.coords.lng, ev);
  }

  openNavigation(){
    this.gameLocationSource(this.game)
    .first()
    .subscribe((data: {latitude, longitude, name}) => {
      window.location = `geo:${data.latitude},${data.longitude};u=35`;
    });
  }

  private gameLocationSource(game: Game) {
    return this.dataStore.getLastTourneyData()
        .pluck('locations')
        .pluck(game.locationId)
        .catch(console.warn.bind(console))
  }
}
