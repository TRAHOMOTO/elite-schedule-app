import {Component, Inject} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Observable} from "rxjs";
import {eliteAppServiceProvider, EliteAppService} from "../../services/";
import {Team} from "../../beans/";

@Component({
  selector: 'page-standings',
  templateUrl: 'standings.html'
})
export class StandingsPage {

  team: Team;
  standings: {divisionName, standings}[];
  //divisionStandings$: Observable<{divisionName, standings}[]>;

  divisionFilter: string = 'division';

  flatStandings: {divisionName, standings}[] = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              @Inject(eliteAppServiceProvider) private dataStore: EliteAppService) {

    this.team = this.navParams.data;
  }

  ionViewWillLoad() {
    this.filterDivision();
    //this.flatStandingsSource().subscribe(data => this.flatStandings = data );
  }

  getHeader(item, idx, records){
    if (idx === 0 || item.division !== records[idx-1].division) {
      return item.division;
    }
    return null;
  }

  filterDivision(){
    this.flatStandingsSource()
        .map(items => items.filter(item => this.divisionFilter === 'all' || item.division === this.team.division))
        .subscribe(data => this.flatStandings = data );
  }

  private flatStandingsSource(): Observable<any>{
    return this.dataStore
        .getLastTourneyData()
        .pluck('standings')
        .switchMap((standings: ArrayLike<any>) => Observable.from(standings))
        .reduce((acc: any[], curr) => [...acc, ...curr], []);

  }

  // private divisionStandingsSource(): Observable<any[]>{
  //   return this.dataStore
  //     .getLastTourneyData()
  //     .pluck('standings')
  //     .switchMap(standings => Observable.from(<ArrayLike<any>>standings))
  //     .groupBy((item:{division}) => item.division)
  //     .flatMap(group => group.reduce((acc, curr) => [...acc, ...curr], []))
  //     .flatMap(grouped => Observable.of(grouped))
  //     .map((groupped) => {
  //       return {
  //         divisionName: groupped[0].division,
  //         standings: groupped
  //       }
  //     })
  //     .reduce((acc: any[], curr) => [...acc, ...curr], [])
  //     .catch(console.error.bind(console));
  // }

}
