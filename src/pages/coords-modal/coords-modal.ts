import {Component} from "@angular/core";
import {NavController, NavParams, ViewController} from "ionic-angular";
import {Geolocation, Geoposition} from '@ionic-native/geolocation';
import {Observable} from "rxjs";

/*
 Generated class for the CoordsModal page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
    selector: 'page-coords-modal',
    templateUrl: 'coords-modal.html'
})
export class CoordsModalPage {

    data$: Observable<any>;
    once: any;
    err: PositionError;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private viewCtrl: ViewController,
                private geo: Geolocation) {
    }

    ionViewDidLoad() {
        this.data$ = this.geo.watchPosition({ enableHighAccuracy: true, timeout:Infinity, maximumAge: 0 })
            .map(data => data.coords).map(coords => {
                return { lat: coords.latitude, lng: coords.longitude}
            });

        this.geo.getCurrentPosition({ enableHighAccuracy: true, timeout:Infinity, maximumAge: 0 })
            .then((data:Geoposition) => {

                this.once =  { lat: data.coords.latitude, lng: data.coords.longitude};
            })
            .catch(err => this.err = err);
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

}
