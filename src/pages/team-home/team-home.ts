import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {TeamDetailsPage, StandingsPage} from "../index";

@Component({
    selector: 'page-team-home',
    templateUrl: 'team-home.html'
})
export class TeamHomePage {

    teamDetailsTab = TeamDetailsPage;
    standingsTab = StandingsPage;

    team: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams) {

        this.team = this.navParams.data;
    }

    goHome(){
        this.navCtrl.popToRoot();
    }

}
