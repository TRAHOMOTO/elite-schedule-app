import {Component, Inject} from '@angular/core';
import {LoadingController, NavController, NavParams} from 'ionic-angular';
import {TeamHomePage} from "../index";
import {eliteAppServiceProvider} from "../../services/index";
import {EliteAppService} from "../../services/elite-app.service";
import {Team, TournamentTeaser} from "../../beans";

import {Observable} from "rxjs";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/groupBy';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/finally';

@Component({
    selector: 'page-teams',
    templateUrl: 'teams.html'
})
export class TeamsPage {

    tournament: TournamentTeaser;
    divisionOfTeams$: Observable<Object[]>;

    queryText = '';

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                @Inject(eliteAppServiceProvider) private dataStore: EliteAppService,
                private loading: LoadingController) {
        this.tournament = this.navParams.data;
    }

    ionViewWillEnter() {

        const loader = this.loading.create({
            content: "Загрузка..."
        });

        loader.present().then(() => {
            this.divisionOfTeams$
                = this.teamDivisionsPipe()
                    .finally(() => loader.dismiss())
                    .catch(console.error.bind(console));
        });
    }

    itemTapped(ev, team) {
        this.navCtrl.push(TeamHomePage, team);
    }

    updateTeams(){
        this.divisionOfTeams$
            = this.teamDivisionsPipe();
    }

    private teamDivisionsPipe(): Observable<any[]>{
        return this.dataStore
            .getTournamentTeams(this.tournament.id)
            .switchMap(teams => Observable.from(teams))
            .filter(this.filterBySearchText.bind(this))
            .groupBy((team) => team.division)
            .flatMap(group => group.reduce((acc: Team[], curr) => [...acc, ...curr], []))
            .flatMap(group => Observable.of(group))
            .map((teamGroup: Team[]) => {
                return {
                    divisionName: teamGroup[0].division,
                    teams: teamGroup
                }
            })
            .reduce((acc: any[], curr) => [...acc, ...curr], []);
    }

    private filterBySearchText(team: Team): boolean {
        if(!this.queryText) {
            return true;
        } else {
            return team.name.toLocaleLowerCase().includes(this.queryText.toLocaleLowerCase())
        }
    }

}
