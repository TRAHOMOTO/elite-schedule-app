import {Component, Inject} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Observable, Subscription} from "rxjs";
import {Game,Team} from "../../beans";
import {TeamHomePage, MapPage} from "../";

import {EliteAppService, eliteAppServiceProvider} from "../../services";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/filter';

declare var window: any;
@Component({
    selector: 'page-game',
    templateUrl: 'game.html'
})
export class GamePage {
    private subscrip: Subscription;
    game: Game;


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                @Inject(eliteAppServiceProvider) private dataStore: EliteAppService) {

        // do nothing
    }

    ionViewWillEnter() {
        this.subscrip = (<Observable<Game>>this.navParams.data)
            .subscribe(d => this.game = d);
    }

    ionViewDidLeave() {
        this.subscrip.unsubscribe();
    }

    teamTapped(teamId: number) {
        this.dataStore.getLastTourneyData()
            .pluck('teams')
            .switchMap((teams: ArrayLike<any>) => Observable.from(teams))
            .filter((team: {id}) => team.id === teamId)
            .map((teamRaw:{ coach, division, id, name }) => new Team(teamRaw))
            .subscribe(team => {
                this.navCtrl.push(TeamHomePage, team);
            });
    }

    goToDirections(){
        this.dataStore.getLastTourneyData()
            .pluck('locations')
            .pluck(this.game.locationId)
            .subscribe((data: {latitude, longitude, name}) => {
                window.location = `geo:${data.latitude},${data.longitude};u=35`;
            });
    }

    goToMap(){
        this.navCtrl.push(MapPage, this.game);
    }

    isWinner(score1, score2){
        return Number(score1) > Number(score2);
    }

}
