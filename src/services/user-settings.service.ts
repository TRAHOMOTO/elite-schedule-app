import {Events} from "ionic-angular";
import { Injectable } from '@angular/core';
import {Storage} from '@ionic/storage';
import {Team} from "../beans/team";
import {BehaviorSubject, Observable, Subject} from "rxjs";

export type FavTeam = {team, tournamentId, tournamentName};

@Injectable()
export class UserSettingsService {

    private favoriteTeams$: Observable<FavTeam[]>;

    constructor(private storage: Storage,
                private events: Events) {

        // do nothing
    }

    favoriteTeam(team: Team,
                 tournamentId: string,
                 tournamentName: string): Promise<void>{

        return this.storage.set(team.id+'', {team, tournamentId, tournamentName})
            .then(() => {
                this.favoriteTeams$ = null;
                this.events.publish('favorites:changed');
            });
    }

    unfavoriteTeam(team: Team): Promise<void> {
        return this.storage.remove(team.id + '')
            .then(() => {
                this.favoriteTeams$ = null;
                this.events.publish('favorites:changed');
            });
    }

    isFavoriteTeam(teamId) {
        return this.storage.get(teamId).then( val => !!val);
    }

    getFavoriteTeams(): Observable<FavTeam[]>{
        if(this.favoriteTeams$){
            return Observable.from(this.favoriteTeams$).first();
        } else {
            const source$ = new Subject<FavTeam[]>();
            const favTeams: FavTeam[] = [];

            this.storage.forEach((value) => {
                favTeams.push(value);
            }).then(() => source$.next(favTeams));


            return source$
                .do((favTeams: FavTeam[])  => {
                    this.favoriteTeams$ = new BehaviorSubject(favTeams);
                });
        }

    }
}
