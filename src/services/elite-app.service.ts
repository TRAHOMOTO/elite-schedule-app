/**
 * Created by traho on 04.04.2017.
 */
import {Injectable} from '@angular/core';
import {Http,Response} from "@angular/http";
import {Observable, BehaviorSubject} from "rxjs";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import {Team, TournamentTeaser} from "../beans";

export type TournamentLikeData = {games, locations, standings, teams, tournament };

@Injectable()
export class EliteAppService {

  private rootApiUrl = 'https://elite-schedule-app-i2-2c4ec.firebaseio.com';

  private lastTourneyData$: Observable<TournamentLikeData>;
  private tourneyDataCache = new Map<string, Observable<TournamentLikeData>>();

  constructor(private http: Http) {
    // do nothing
  }

  getTournaments(): Observable<TournamentTeaser[]> {
    return this.http.get(`${this.rootApiUrl}/tournaments.json`)
        .map((response: Response) => response.json())
        .map(tourneys => tourneys.map(tourney => new TournamentTeaser(tourney)));
  }


    getTournamentTeams(tourneyID: string,
                       useCache: boolean = true): Observable<Team[]>{

      return this.getTournamentData(tourneyID, useCache)
          .pluck('teams')
          .map((data: Array<any> ) => data.map((team) => new Team(team)));
    }

    private newBehavirLastData(data){
        this.lastTourneyData$ = new BehaviorSubject(data)
    }

  getTournamentData(tourneyID: string,
                    useCache: boolean = true ): Observable<TournamentLikeData>{

      if(useCache && this.tourneyDataCache.has(tourneyID)){

          return Observable.from(this.tourneyDataCache.get(tourneyID))
              .do(this.newBehavirLastData.bind(this))
              .first();

      } else if( useCache && !this.tourneyDataCache.has(tourneyID)){

          return this.http
              .get(`${this.rootApiUrl}/tournaments-data/${tourneyID}.json`)
              .map((response: Response) => response.json())
              .do(this.newBehavirLastData.bind(this))
              .do(() => this.tourneyDataCache.set(tourneyID, this.lastTourneyData$));

      } else if( !useCache ){

          return this.http
              .get(`${this.rootApiUrl}/tournaments-data/${tourneyID}.json`)
              .map((response: Response) => response.json())
              .do(this.newBehavirLastData.bind(this))
              .do(() => this.tourneyDataCache.set(tourneyID, this.lastTourneyData$));
      }
  }

  getLastTourneyData(): Observable<TournamentLikeData>{
    return Observable.from(this.lastTourneyData$).first();
  }

  refreshLastTourneyData(): Observable<TournamentLikeData>{
      return this.getLastTourneyData().switchMap(
          (data: TournamentLikeData) => this.getTournamentData(data.tournament.id, false).first()
      );
  }
}
