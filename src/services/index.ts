import {OpaqueToken} from "@angular/core";

export * from './elite-app.service';
export * from './user-settings.service';

export const eliteAppServiceProvider = new OpaqueToken('eliteAppServiceProvider');
