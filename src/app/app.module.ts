import {ErrorHandler, NgModule} from "@angular/core";
import {IonicApp, IonicErrorHandler, IonicModule} from "ionic-angular";
import {IonicStorageModule} from "@ionic/storage";
import { Vibration } from '@ionic-native/vibration';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { LocalNotifications } from '@ionic-native/local-notifications';

import {MyApp} from "./app.component";

import {StatusBar} from "@ionic-native/status-bar";
import {SplashScreen} from "@ionic-native/splash-screen";

import {AgmCoreModule} from 'angular2-google-maps/core';

import {
    GamePage,
    MapPage,
    MyTeamsPage,
    StandingsPage,
    TeamDetailsPage,
    TeamHomePage,
    TeamsPage,
    TournamentsPage,
    CoordsModalPage
} from "../pages";
import {EliteAppService, eliteAppServiceProvider, UserSettingsService} from "../services";

@NgModule({
    declarations: [
        MyApp,
        MyTeamsPage,
        TournamentsPage,
        TeamsPage,
        TeamDetailsPage,
        TeamHomePage,
        StandingsPage,
        GamePage,
        MapPage,
        CoordsModalPage
    ],
    imports: [
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot({
            name: 'elite_schedule_app',
            driverOrder: ['indexeddb', 'sqlite', 'websql']
        }),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDDwJa2xeHngIh-WaC7whY3bJHUCqv-kYM'
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        MyTeamsPage,
        TournamentsPage,
        TeamsPage,
        TeamDetailsPage,
        TeamHomePage,
        StandingsPage,
        GamePage,
        MapPage,
        CoordsModalPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        {provide: eliteAppServiceProvider, useClass: EliteAppService},
        UserSettingsService,
        Vibration, Geolocation, Camera, LocalNotifications
    ]
})
export class AppModule {
}
