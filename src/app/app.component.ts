import {Component, Inject, ViewChild} from '@angular/core';
import {Events, LoadingController, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {MyTeamsPage, TournamentsPage, TeamHomePage} from '../pages/index';
import {FavTeam, UserSettingsService, eliteAppServiceProvider, EliteAppService} from "../services/";
import {Team} from "../beans/";

import {Observable} from "rxjs";


@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = MyTeamsPage;
    favTeamsSource$: Observable<FavTeam[]>;

    constructor(public platform: Platform,
                public statusBar: StatusBar,
                public splashScreen: SplashScreen,
                private userSettings: UserSettingsService,
                private events: Events,
                private loading: LoadingController,
                @Inject(eliteAppServiceProvider) private dataStore: EliteAppService) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();

            this.refreshFavTeams();
            this.events.subscribe('favorites:changed', () => this.refreshFavTeams());
        });
    }

    goHome() {
        this.nav.push(MyTeamsPage);
    }

    goToTournaments(){
        this.nav.push(TournamentsPage)
    }

    goToTeamDetails(favItem: FavTeam){
        this.loading.create({
            content: 'Загрузка ...',
            dismissOnPageChange: true
        })
            .present()
            .then(() => {
                this.dataStore.getTournamentData(favItem.tournamentId)
                    .subscribe(() => {
                        this.nav.push(TeamHomePage, new Team(favItem.team))
                    })
            });
        //this.nav.push(TeamDetailsPage, favTeam.team);
    }


    private refreshFavTeams(){
        this.favTeamsSource$ = this.userSettings.getFavoriteTeams();
    }
}
